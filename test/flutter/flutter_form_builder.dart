import "package:flutter/material.dart";
import "package:flutter/widgets.dart";
import "package:flutter_test/flutter_test.dart";
import "package:json_schema/json_schema.dart";
import "package:schema_form/flutter/form_builder.dart";
import "package:schema_form/flutter/widgets/array_widget.dart";
import "package:schema_form/flutter/widgets/boolean_widget.dart";
import "package:schema_form/flutter/widgets/form_widget.dart";
import "package:schema_form/flutter/widgets/string_widget.dart";

String errorText(String value) => value + "/error";

void main() {
  testWidgets(
    "String Widget made from Simple String Schema",
    (WidgetTester tester) async {
      const WidgetFormBuilder formBuilder = WidgetFormBuilder();
      await tester.pumpWidget(
        Builder(
          builder: (BuildContext context) {
            final Widget built = formBuilder.build(
              context,
              JsonSchema.createSchema(
                {"type": "string"},
              ),
              false,
            );

            expect(built is StringWidget, true);

            return const Placeholder();
          },
        ),
      );
    },
  );

  testWidgets(
    "Form with String Widget handles validation",
    (WidgetTester tester) async {
      const WidgetFormBuilder formBuilder = WidgetFormBuilder();
      const int maxLength = 3;
      const String badString = "oh noes";
      const String goodString = "yup";

      await tester.pumpWidget(
        MaterialApp(
          home: MediaQuery(
            data: const MediaQueryData(devicePixelRatio: 1.0),
            child: Directionality(
              textDirection: TextDirection.ltr,
              child: Center(
                child: Material(
                  child: Builder(
                    builder: (BuildContext context) {
                      final Widget built = formBuilder.build(
                        context,
                        JsonSchema.createSchema(
                          {
                            "type": "string",
                            "maxLength": maxLength,
                          },
                        ),
                      );

                      expect(built is FormWidget, true);

                      expect((built as FormWidget).child is StringWidget, true);

                      return built;
                    },
                  ),
                ),
              ),
            ),
          ),
        ),
      );

      await tester.enterText(find.byType(TextField), badString);

      await tester.pump();

      expect(
        find.text("maxLength exceeded ($badString vs $maxLength)"),
        findsOneWidget,
      );

      await tester.enterText(find.byType(TextField), goodString);

      await tester.pump();

      expect(
        find.text("maxLength exceeded ($badString vs $maxLength)"),
        findsNothing,
      );

      expect(
        find.text("maxLength exceeded ($goodString vs $maxLength)"),
        findsNothing,
      );
    },
  );

  testWidgets(
    "Boolean Widget made from Simple Boolean Schema",
    (WidgetTester tester) async {
      const WidgetFormBuilder formBuilder = WidgetFormBuilder();
      await tester.pumpWidget(
        Builder(
          builder: (BuildContext context) {
            final Widget built = formBuilder.build(
              context,
              JsonSchema.createSchema(
                {"type": "boolean"},
              ),
              false,
            );

            expect(built is BooleanWidget, true);

            return const Placeholder();
          },
        ),
      );
    },
  );

  testWidgets(
    "Array Widget made from Simple Array Schema",
    (WidgetTester tester) async {
      const WidgetFormBuilder formBuilder = WidgetFormBuilder();
      await tester.pumpWidget(
        Builder(
          builder: (BuildContext context) {
            final Widget built = formBuilder.build(
              context,
              JsonSchema.createSchema(
                {
                  "type": "array",
                  "items": {
                    "type": "object",
                  },
                },
              ),
              false,
            );

            expect(built is ArrayWidget, true);

            return const Placeholder();
          },
        ),
      );
    },
  );
}
