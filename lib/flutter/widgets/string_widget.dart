import "package:flutter/material.dart";
import "package:json_schema/json_schema.dart";
import "package:schema_form/form_builder.dart";

class StringWidget extends StatefulWidget {
  final JsonSchema schema;

  const StringWidget({
    Key key,
    @required this.schema,
  })  : assert(schema != null),
        super(key: key);

  @override
  _StringWidgetState createState() => _StringWidgetState();
}

class _StringWidgetState extends State<StringWidget> {
  TextEditingController _controller;

  @override
  Widget build(BuildContext context) => TextFormField(
        key: PageStorageKey(widget.schema.path),
        controller: _controller,
        decoration: InputDecoration(
          labelText: widget.schema.title,
        ),
        validator: (value) => validationErrorText(
          widget.schema,
          value,
        ),
      );

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController(
      text: widget.schema.defaultValue?.toString() ?? "",
    );
  }
}
