import "package:flutter/material.dart";

class ArrayWidget extends StatefulWidget {
  final Widget items;

  const ArrayWidget({
    Key key,
    this.items,
  }) : super(key: key);

  @override
  _ArrayWidgetState createState() => _ArrayWidgetState();
}

class _ArrayWidgetState extends State<ArrayWidget> {
  @override
  Widget build(BuildContext context) => Column(
        children: <Widget>[
          _buildArrayValue(),
          _buildArrayValue(),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              IconButton(
                icon: const Icon(Icons.add_circle),
                onPressed: () {},
              ),
            ],
          )
        ],
      );

  Widget _buildArrayValue() => Card(
        child: Row(
          children: <Widget>[
            Expanded(
              child: widget.items,
            ),
            Column(
              children: <Widget>[
                IconButton(
                  icon: const Icon(Icons.arrow_upward),
                  onPressed: () {},
                ),
                IconButton(
                  icon: const Icon(Icons.arrow_downward),
                  onPressed: () {},
                ),
                IconButton(
                  icon: const Icon(Icons.remove_circle),
                  onPressed: () {},
                ),
              ],
            ),
          ],
        ),
      );
}
