import "package:flutter/widgets.dart";

class FormWidget extends StatefulWidget {
  final Widget child;

  const FormWidget({
    Key key,
    @required this.child,
  })  : assert(child != null),
        super(key: key);

  @override
  FormWidgetState createState() => FormWidgetState();
}

class FormWidgetState extends State<FormWidget> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(
    BuildContext context,
  ) =>
      Form(
        key: _formKey,
        autovalidate: true,
        child: widget.child,
      );
}
