import "package:flutter/material.dart";
import "package:json_schema/json_schema.dart";
import "package:schema_form/form_builder.dart";

class BooleanWidget extends StatefulWidget {
  final JsonSchema schema;

  const BooleanWidget({
    Key key,
    this.schema,
  }) : super(key: key);

  @override
  _BooleanWidgetState createState() => _BooleanWidgetState();
}

class _BooleanWidgetState extends State<BooleanWidget> {
  bool _value;

  @override
  Widget build(BuildContext context) => FormField<bool>(
        builder: (FormFieldState state) => SwitchListTile(
          title: Text(widget.schema.title),
          value: _value,
          onChanged: (bool value) {
            setState(() {
              _value = value;
              state.didChange(value);
            });
          },
        ),
        validator: (value) => validationErrorText(
          widget.schema,
          value,
        ),
      );

  @override
  void initState() {
    super.initState();
    _value = true;
  }
}
