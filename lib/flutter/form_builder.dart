import "package:flutter/material.dart";
import "package:flutter/widgets.dart";
import "package:json_schema/json_schema.dart";
import "package:quiver/strings.dart";
import "package:schema_form/flutter/widgets/array_widget.dart";
import "package:schema_form/flutter/widgets/boolean_widget.dart";
import "package:schema_form/flutter/widgets/form_widget.dart";
import "package:schema_form/flutter/widgets/string_widget.dart";
import "package:schema_form/form_builder.dart";

// TODO: The flutter directory should eventually be broken out to it's own lib.
class WidgetFormBuilder extends FormBuilder<Widget, BuildContext> {
  const WidgetFormBuilder();

  @override
  Widget array(
    BuildContext context,
    JsonSchema schema,
  ) =>
      ArrayWidget(
        key: Key(schema.path),
        items: build(context, schema.items),
      );

  @override
  Widget boolean(
    BuildContext context,
    JsonSchema schema,
  ) =>
      BooleanWidget(
        key: Key(schema.path),
        schema: schema,
      );

  @override
  Widget form(
    BuildContext context,
    JsonSchema schema,
    Widget child,
  ) =>
      FormWidget(
        key: Key(schema.path),
        child: child,
      );

  // TODO: implement
  @override
  Widget integer(
    BuildContext context,
    JsonSchema schema,
  ) =>
      const Text("TODO: integer");

  // TODO: implement
  @override
  Widget nullValue(
    BuildContext context,
    JsonSchema schema,
  ) =>
      const Text("TODO: nullValue");

  // TODO: implement
  @override
  Widget number(
    BuildContext context,
    JsonSchema schema,
  ) =>
      const Text("TODO: number");

  @override
  Widget object(
    BuildContext context,
    JsonSchema schema,
    Iterable<Widget> children,
  ) =>
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          if (isNotBlank(schema.title))
            Text(
              schema.title,
              style: Theme.of(context).textTheme.title,
            ),
          ...children ?? [],
        ],
      );

  @override
  Widget string(
    BuildContext context,
    JsonSchema schema,
  ) =>
      StringWidget(
        key: Key(schema.path),
        schema: schema,
      );
}
