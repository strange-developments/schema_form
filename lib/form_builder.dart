import "package:flutter/material.dart";
import "package:flutter/widgets.dart";
import "package:json_schema/json_schema.dart";

@immutable
abstract class FormBuilder<T, C> {
  const FormBuilder();

  @protected
  T array(
    C context,
    JsonSchema schema,
  );

  @protected
  T boolean(
    C context,
    JsonSchema schema,
  );

  T build(
    C context,
    JsonSchema schema, [
    bool formWrap = true,
  ]) {
    T child;
    switch (schema?.type) {
      case SchemaType.array:
        child = array(context, schema);
        break;
      case SchemaType.boolean:
        child = boolean(context, schema);
        break;
      case SchemaType.integer:
        child = integer(context, schema);
        break;
      case SchemaType.nullValue:
        child = nullValue(context, schema);
        break;
      case SchemaType.number:
        child = number(context, schema);
        break;
      case SchemaType.string:
        child = string(context, schema);
        break;
      case SchemaType.object:
        child = object(
          context,
          schema,
          schema.properties.entries
              .map(
                (e) => build(context, schema, false),
              )
              .where((e) => e != null),
        );
        break;
      default:
        return null;
    }
    if (formWrap) {
      return form(context, schema, child);
    }
    return child;
  }

  @protected
  T form(
    C context,
    JsonSchema schema,
    T child,
  );

  @protected
  T integer(
    C context,
    JsonSchema schema,
  );

  @protected
  T nullValue(
    C context,
    JsonSchema schema,
  );

  @protected
  T number(
    C context,
    JsonSchema schema,
  );

  @protected
  T object(
    C context,
    JsonSchema schema,
    Iterable<T> children,
  );

  @protected
  T string(
    C context,
    JsonSchema schema,
  );
}

String validationErrorText(JsonSchema schema, dynamic value) {
  final List<ValidationError> errors = schema.validateWithErrors(value);
  if (errors != null && errors.isNotEmpty) {
    return errors.map((e) => e.message).join("\n");
  }
  return null;
}
